package cn.donting.spring.boot.server;

import cn.donting.spring.boot.plugin.core.server.PluginServerApplication;

import java.lang.reflect.Method;
import java.net.URLClassLoader;

/**
 * @author donting
 * 2020-09-24 21:12
 */
public class PluginServer {

    public static void main(String[] args) throws Exception {
        PluginServerApplication.runServer(args);
    }
}
