package cn.donting.spring.boot.starter.context;

import cn.donting.spring.boot.plugin.core.logging.Log;
import cn.donting.spring.boot.plugin.core.logging.LogFactory;
import cn.donting.spring.boot.plugin.core.plugin.PluginSpringApplicationContext;
import cn.donting.spring.boot.plugin.core.plugin.PluginWebSpringApplicationContext;
import cn.donting.spring.boot.plugin.core.plugin.manger.PluginManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootVersion;
import org.springframework.boot.web.servlet.context.AnnotationConfigServletWebServerApplicationContext;
import org.springframework.core.SpringVersion;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * manager 是 web 的context
 *
 * @author donting
 * 2020-09-25 13:08
 */
public class PluginManagerAnnotationConfigServletWebServerApplicationContext
        extends AnnotationConfigServletWebServerApplicationContext
        implements PluginWebSpringApplicationContext {
    private static final Log log = LogFactory.getLogger(PluginManagerAnnotationConfigServletWebServerApplicationContext.class);

    private Class<?> mainClass;

    private String tempPath;

    @Override
    public String getSpringBootVersion() {
        return SpringBootVersion.getVersion();
    }


    @Override
    @Deprecated
    public void doService(HttpServletRequest request, HttpServletResponse response) throws Exception {
        log.error("doService is  Deprecated");
    }

    @Override
    public void exit() {
        SpringApplication.exit(this);
    }

    @Override
    public Class<?> getMainClass() {
        return mainClass;
    }

    @Override
    public void setMainClass(Class<?> mainClass) {
        this.mainClass = mainClass;
    }

    @Override
    public String getTempPath() {
        return tempPath;
    }

    @Override
    public void setTempPath(String tempPath) {
        if (this.tempPath == null) {
            this.tempPath = tempPath;
        } else {
            log.warning("tempPath is set");
        }
    }
}
