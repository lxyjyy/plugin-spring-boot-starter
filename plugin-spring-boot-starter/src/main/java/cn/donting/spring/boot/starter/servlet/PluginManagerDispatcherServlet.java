package cn.donting.spring.boot.starter.servlet;

import cn.donting.spring.boot.plugin.core.plugin.PluginBootWebWrapper;
import cn.donting.spring.boot.plugin.core.plugin.PluginWrapper;
import cn.donting.spring.boot.plugin.core.plugin.manger.PluginBootWebManager;
import cn.donting.spring.boot.plugin.core.servlet.IPluginManagerDispatcherServlet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author donting
 * 2020-09-25 09:29
 */
public class PluginManagerDispatcherServlet extends DispatcherServlet implements IPluginManagerDispatcherServlet {
    protected static final Log log = LogFactory.getLog(PluginManagerDispatcherServlet.class);

    private PluginBootWebManager pluginBootWebManager;

    public PluginManagerDispatcherServlet(PluginBootWebManager pluginBootWebManager) {
        this.pluginBootWebManager=pluginBootWebManager;
    }


    @Override
    public void doService(HttpServletRequest request, HttpServletResponse response) throws Exception {
//        super.doService(request, response);
        String pluginId = request.getParameter("plugin");
        if (pluginId == null) {
            log.warn("pluginId is null");
            response.setStatus(404);
            return;
        }
        if (pluginId.equals(pluginBootWebManager.getId())) {
            super.doService(request, response);
            return;
        }
        PluginWrapper running = pluginBootWebManager.getRunning(pluginId);
        if (running != null) {
            if (running instanceof PluginBootWebWrapper) {
                ((PluginBootWebWrapper) running).doService(request, response);
            } else {
                log.warn(pluginId + "is not web");
            }
        } else {
            log.warn(pluginId + " not find");
            response.setStatus(404);
        }

    }


}
