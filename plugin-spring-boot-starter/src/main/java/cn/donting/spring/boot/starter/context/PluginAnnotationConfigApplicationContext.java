package cn.donting.spring.boot.starter.context;

import cn.donting.spring.boot.plugin.core.logging.Log;
import cn.donting.spring.boot.plugin.core.logging.LogFactory;
import cn.donting.spring.boot.plugin.core.plugin.PluginMainClass;
import cn.donting.spring.boot.plugin.core.plugin.PluginSpringApplicationContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootVersion;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.SpringVersion;

/**
 * 插件/manager 不是web 的context
 *
 * @author donting
 * 2020-09-25 13:10
 */
public class PluginAnnotationConfigApplicationContext
        extends AnnotationConfigApplicationContext
        implements PluginSpringApplicationContext, PluginMainClass {
    private static final Log log = LogFactory.getLogger(PluginAnnotationConfigApplicationContext.class);

    private Class<?> mainClass;

    private String tempPath;

    @Override
    public String getSpringBootVersion() {
        return SpringBootVersion.getVersion();
    }

    @Override
    public void exit() {
        SpringApplication.exit(this);
    }

    @Override
    public Class<?> getMainClass() {
        return mainClass;
    }

    @Override
    public void setMainClass(Class<?> mainClass) {
        this.mainClass = mainClass;
    }
    @Override
    public String getTempPath() {
        return tempPath;
    }

    @Override
    public void setTempPath(String tempPath) {
        if (this.tempPath == null) {
            this.tempPath = tempPath;
        } else {
            log.warning("tempPath is set");
        }
    }
}
