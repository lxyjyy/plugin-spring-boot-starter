package cn.donting.spring.boot.starter.context;

import cn.donting.spring.boot.plugin.core.logging.Log;
import cn.donting.spring.boot.plugin.core.logging.LogFactory;
import cn.donting.spring.boot.plugin.core.plugin.DoService;
import cn.donting.spring.boot.plugin.core.plugin.PluginSpringApplicationContext;
import cn.donting.spring.boot.plugin.core.plugin.PluginWebSpringApplicationContext;
import cn.donting.spring.boot.plugin.core.servlet.IPluginDispatcherServlet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootVersion;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.SpringVersion;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 插件是web 的Context
 *
 * @author donting
 * 2020-09-25 13:10
 */
public class PluginWebAnnotationConfigApplicationContext
        extends AnnotationConfigApplicationContext
        implements PluginWebSpringApplicationContext, DoService {
    private static final Log log = LogFactory.getLogger(PluginWebAnnotationConfigApplicationContext.class);

    private IPluginDispatcherServlet iPluginDispatcherServlet;
    private Class<?> mainClass;
    private String tempPath;

    @Override
    public String getSpringBootVersion() {
        return SpringBootVersion.getVersion();
    }


    @Override
    public void doService(HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (iPluginDispatcherServlet == null) {
            IPluginDispatcherServlet bean = getBean(IPluginDispatcherServlet.class);
            iPluginDispatcherServlet = bean;
        }
        iPluginDispatcherServlet.doService(request, response);
    }

    @Override
    public void exit() {
        SpringApplication.exit(this);
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("PluginWebAnnotationConfigApplicationContext finalize");
    }

    @Override
    public Class<?> getMainClass() {
        return mainClass;
    }

    @Override
    public void setMainClass(Class<?> mainClass) {
        this.mainClass = mainClass;
    }
    @Override
    public String getTempPath() {
        return tempPath;
    }

    @Override
    public void setTempPath(String tempPath) {
        if (this.tempPath == null) {
            this.tempPath = tempPath;
        } else {
            log.warning("tempPath is set");
        }
    }
}
