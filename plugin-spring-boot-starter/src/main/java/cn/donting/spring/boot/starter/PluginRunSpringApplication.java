package cn.donting.spring.boot.starter;

import cn.donting.spring.boot.plugin.core.PluginSpringApplicationRun;
import cn.donting.spring.boot.plugin.core.loader.classes.PluginManagerClassLoader;
import cn.donting.spring.boot.plugin.core.plugin.PluginSpringApplicationContext;
import cn.donting.spring.boot.plugin.core.plugin.manger.PluginBootManager;

/**
 * @author donting
 * 2020-09-29 11:05
 */
public class PluginRunSpringApplication implements PluginSpringApplicationRun {

    private PluginSpringApplicationContext pluginSpringApplicationContext;
    private PluginSpringApplication pluginSpringApplication;
    private Class<?> mainClass;
    private String[] args;

    @Override
    public PluginSpringApplicationContext getNotRefreshPluginSpringApplicationContext() {
        return pluginSpringApplicationContext;
    }

    @Override
    public void init(Class<?> mainClass, String[] args) {
        this.mainClass = mainClass;
        this.args = args;
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        if (PluginBootManager.isWeb(contextClassLoader)) {
            this.pluginSpringApplication = new PluginSpringApplication(PluginSpringApplication.Type.WEB, mainClass);
        } else {
            this.pluginSpringApplication = new PluginSpringApplication(PluginSpringApplication.Type.NONE, mainClass);
        }
        pluginSpringApplicationContext = pluginSpringApplication.getApplicationContext();
    }

    @Override
    public PluginSpringApplicationContext run() {
//        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
//        if (contextClassLoader instanceof PluginManagerClassLoader) {
//            return runManager(mainClass, args);
//        } else {
//            return runPlugin(mainClass, args);
//        }
        PluginSpringApplicationContext run = (PluginSpringApplicationContext) pluginSpringApplication.run(args);
        return run;
    }

    private PluginSpringApplicationContext runManager(Class<?> mainClass, String[] args) {
        PluginSpringApplicationContext run = (PluginSpringApplicationContext) pluginSpringApplication.run(args);
        return run;

    }


    private PluginSpringApplicationContext runPlugin(Class<?> mainClass, String[] args) {
        PluginSpringApplicationContext run = (PluginSpringApplicationContext) pluginSpringApplication.run(args);
        return run;
    }
}
