package test.plugin.manager;

import cn.donting.spring.boot.plugin.core.plugin.TempDirectory;
import cn.donting.spring.boot.plugin.core.plugin.manger.PluginManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.swing.*;
import java.util.HashMap;

/**
 * @author donting
 * 2020-09-24 22:24
 */
@RestController
public class Controller {

    @Autowired
    PluginManager pluginManager;
    @Autowired
    TempDirectory tempDirectory;

    public Controller() {
        System.out.println("----------");
    }

    @GetMapping("/test")
    public HashMap<String, Object> test() {
        String tempPath = tempDirectory.getTempPath();
        System.out.println(tempPath);
        return new HashMap<String, Object>() {{
            put("a", 123);
            put("b", "哈哈哈哈👌");
        }};
    }

    @PostMapping("/file")
    public HashMap<String, Object> file(@RequestParam("file") MultipartFile multipartFile) {
        System.out.println();
        return new HashMap<String, Object>() {{
            put("a", 123);
            put("b", "哈哈哈哈👌");
            put("file", multipartFile.getOriginalFilename());
        }};
    }


}
