package test.plugin.manager;

import cn.donting.spring.boot.plugin.core.Plugin;
import cn.donting.spring.boot.starter.PluginSpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.DispatcherServletAutoConfiguration;

/**
 * @author donting
 * 2020-09-24 20:53
 */
@SpringBootApplication(exclude = DispatcherServletAutoConfiguration.class)
@cn.donting.spring.boot.plugin.core.Manager
@Plugin(id = "sys",name = "管理者",versionCode = 1,version = "1.0")
public class Manager {

    public static void main(String[] args) {
        PluginSpringApplication.runApplication(Manager.class,args);
//        SpringApplication springApplication=new SpringApplication();
//        PluginApplication.run(Manager.class,args);
    }
}
