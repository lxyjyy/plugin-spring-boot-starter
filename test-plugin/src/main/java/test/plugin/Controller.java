package test.plugin;

import cn.donting.spring.boot.plugin.core.plugin.TempDirectory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * @author donting
 * 2020-09-24 22:24
 */
@RestController
public class Controller {

    @Autowired
    TempDirectory tempDirectory;

    @GetMapping("/test")
    public HashMap<String, Object> test(HttpServletRequest request) {
        Class<? extends HttpServletRequest> aClass = request.getClass();
        String tempPath = tempDirectory.getTempPath();
        return new HashMap<String, Object>() {{
            put("a", 123);
            put("b", "哈哈哈哈");
            put("c", "我是插件");
            put("d", request.getRequestURI()
            );
        }};
    }
    @PostMapping("/file")
    public HashMap<String,Object> file(@RequestParam("file")MultipartFile[] multipartFile,String id){
        System.out.println();

        return new HashMap<String,Object>(){{
            put("a",123);
            put("b","哈哈哈哈👌");
            put("file",multipartFile[0].getOriginalFilename());
        }};
    }

}
