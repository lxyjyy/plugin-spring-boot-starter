package cn.donting.spring.boot.plugin.core.plugin;

/**
 * @author donting
 * 2020-09-29 14:05
 */
public class TempPath implements TempDirectory{
    private final String path;

    public TempPath(String path) {
        this.path = path;
    }

    @Override
    public String getTempPath() {
        return path;
    }
}
