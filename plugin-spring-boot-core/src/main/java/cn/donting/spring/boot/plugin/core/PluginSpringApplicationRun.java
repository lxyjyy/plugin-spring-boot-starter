package cn.donting.spring.boot.plugin.core;

import cn.donting.spring.boot.plugin.core.plugin.PluginSpringApplicationContext;

/**
 * @author donting
 * 2020-09-29 11:01
 */
public interface PluginSpringApplicationRun {

     PluginSpringApplicationContext getNotRefreshPluginSpringApplicationContext();

     void init(Class<?> mainClass,String[] args);
     PluginSpringApplicationContext run();
}
