package cn.donting.spring.boot.plugin.core.loader.plugin.dev;

import cn.donting.spring.boot.plugin.core.loader.CommonLib;
import cn.donting.spring.boot.plugin.core.loader.plugin.PluginLoader;
import cn.donting.spring.boot.plugin.core.loader.plugin.PluginManagerLoader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;

/**
 * @author donting
 * 2020-09-25 11:16
 */
public abstract class AbsDevLoader  {

    final ClassPathEntity creatURLForClassPath(File file) throws IOException, ReflectiveOperationException {
        ArrayList<URL> arrayList = new ArrayList<>();
        ArrayList<URL> commonLibList = new ArrayList<>();
        FileInputStream fileInputStream = new FileInputStream(file);
        byte[] bytes = new byte[fileInputStream.available()];
        fileInputStream.read(bytes);
        String s = new String(bytes);
        String[] urlPaths = s.split("\n");
        for (int i = 1; i < urlPaths.length; i++) {
            if (CommonLib.isCommonLib(urlPaths[i])) {
                commonLibList.add(new URL(urlPaths[i]));
            }else {
                arrayList.add(new URL(urlPaths[i]));
            }
        }
        URL[] urls = new URL[arrayList.size()];
        urls = arrayList.toArray(urls);

        URL[] common = new URL[commonLibList.size()];
        common = commonLibList.toArray(common);
        return new ClassPathEntity(urls,common, urlPaths[0]);
    }


    static class ClassPathEntity {
        private URL[] urls;
        private URL[] commonLib;
        private String mainClass;

        public ClassPathEntity(URL[] urls, URL[] commonLib, String mainClass) {
            this.urls = urls;
            this.commonLib = commonLib;
            this.mainClass = mainClass;
        }

        public URL[] getCommonLib() {
            return commonLib;
        }

        public String getMainClass() {
            return mainClass;
        }

        public URL[] getUrls() {
            return urls;
        }
    }
}