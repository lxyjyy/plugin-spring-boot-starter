package cn.donting.spring.boot.plugin.core.plugin;

import java.util.Map;

/**
 * plugin 对spring 容器的 接口
 *
 * @author donting
 * 2020-09-23 12:13
 */
public interface PluginSpringApplicationContext extends TempDirectory {
    <T> T getBean(Class<T> type) throws Exception;

    <T> Map<String, T> getBeansOfType(Class<T> type) throws Exception;

    Object getBean(String name) throws Exception;

    String getSpringBootVersion();

    void setTempPath(String path);

    default void exit() {
    }
}
