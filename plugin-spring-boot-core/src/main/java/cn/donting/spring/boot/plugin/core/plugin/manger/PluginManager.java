package cn.donting.spring.boot.plugin.core.plugin.manger;

import cn.donting.spring.boot.plugin.core.exception.PluginException;
import cn.donting.spring.boot.plugin.core.loader.plugin.PluginLoader;
import cn.donting.spring.boot.plugin.core.plugin.PluginWrapper;
import cn.donting.spring.boot.plugin.core.plugin.TempDirectory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * 插件manager
 * @author donting
 * 2020-09-14 16:41
 */
public interface PluginManager extends TempDirectory {

    String PLUGIN_BASE = System.getProperty("user.dir") + File.separator + "plugin";
    String PLUGIN_INSTALL_BASE = PLUGIN_BASE + File.separator + "plugin";
    String PLUGIN_TEMP = PLUGIN_BASE + File.separator + "temp" + File.separator + "plugin";
    PluginWrapper getRunning(String pluginId);
    void loadDevPlugin() throws Exception;
    void stop(String pluginId) throws Exception;
    void start(String pluginId) throws Exception;
    default String install(File file) throws Exception {
        FileInputStream fileInputStream = new FileInputStream(file);
        byte[] bytes = new byte[fileInputStream.available()];
        fileInputStream.read(bytes);
        return install(bytes);
    }
    String install(byte[] bytes) throws Exception;
    PluginLoader getPluginLoader(File file);
    String getId();
   default void  update(File file,String pluginId) throws Exception {
       FileInputStream fileInputStream = new FileInputStream(file);
       byte[] bytes = new byte[fileInputStream.available()];
       fileInputStream.read(bytes);
       update(bytes,pluginId);
   }
    void update(byte[] bytes,String pluginId) throws Exception;

    /**
     * 是否初始化
     * @return
     */
   default boolean initializing(){
       String tempPath = getTempPath();
       if(tempPath==null){
           return true;
       }
       return false;
    }
}
