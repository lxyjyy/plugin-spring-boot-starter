package cn.donting.spring.boot.plugin.core.plugin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * http doService
 *
 * @author donting
 * 2020-09-25 11:35
 */
public interface DoService {
    void doService(HttpServletRequest request, HttpServletResponse response) throws Exception;
}
