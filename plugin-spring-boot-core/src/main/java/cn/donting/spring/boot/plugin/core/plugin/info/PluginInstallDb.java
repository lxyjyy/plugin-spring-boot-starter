package cn.donting.spring.boot.plugin.core.plugin.info;



import cn.donting.spring.boot.plugin.core.plugin.manger.PluginManager;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

/**
 * 插件 安装信息 接口
 * @author donting
 * 2020-08-14 09:07
 */
public interface PluginInstallDb {

    /**
     * 安装
     * @param pluginInfo PluginInfo
     * @param filePath 插件文件地址
     * @param tempPath 插件缓存目录地址
     * @return PluginInstallInfo
     * @throws IOException
     */
    PluginInstallInfo install(PluginInfo pluginInfo, String filePath,String tempPath) throws IOException;

    /**
     * 插件更新
     * @param pluginInfo  PluginInfo
     * @return PluginInstallInfo
     * @throws IOException
     */
    PluginInstallInfo update(PluginInfo pluginInfo) throws IOException;

    /**
     * 卸载
     * @param pluginId id
     * @return PluginInstallInfo
     * @throws IOException
     */
    PluginInstallInfo uninstall(String pluginId) throws IOException;

    /**
     * 停止
     * @param pluginId id
     * @return PluginInstallInfo
     * @throws IOException
     */
    PluginInstallInfo stop(String pluginId) throws IOException;

    /**
     * 启动
     * @param pluginId pluginId
     * @return PluginInstallInfo
     * @throws IOException
     */
    PluginInstallInfo start(String pluginId) throws IOException;

    /**
     * 获取安装信息
     * @param pluginId pluginId
     * @return PluginInstallInfo
     * @throws IOException
     */
    PluginInstallInfo get(String pluginId) throws IOException;

    void update(PluginInstallInfo pluginInstallInfo) throws IOException;

    /**
     * 获取所有的安装信息
     * @return  HashMap< pluginId, PluginInstallInfo>
     * @throws IOException
     */
    HashMap<String, PluginInstallInfo> getAll() throws IOException;
}
