package cn.donting.spring.boot.plugin.core.plugin.info;

import cn.donting.spring.boot.plugin.core.logging.Log;
import cn.donting.spring.boot.plugin.core.logging.LogFactory;
import cn.donting.spring.boot.plugin.core.plugin.PluginStatus;
import cn.donting.spring.boot.plugin.core.plugin.manger.PluginManager;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * @author donting
 * 2020-08-14 09:11
 */
public class DefaultPluginInstallDb implements PluginInstallDb {

    private static final Log log = LogFactory.getLogger(DefaultPluginInstallDb.class);
    private static final String dbPath = PluginManager.PLUGIN_BASE + File.separator + "installInfo.json";

    static {
        File file = new File(dbPath);
        if (!file.exists()) {
            try {
                file.createNewFile();
                new DefaultPluginInstallDb().write(new Db());

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public PluginInstallInfo install(PluginInfo plugin, String filePath, String temp) throws IOException {
        PluginInstallInfo pluginInstallInfo = new PluginInstallInfo(plugin);
        pluginInstallInfo.setTemp(temp);
        pluginInstallInfo.setStatus(PluginStatus.STOP);
        pluginInstallInfo.setFilePath(filePath);
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = sdf1.format(new Date());
        pluginInstallInfo.setInstallTime(format);
        Db read = read();
        read.plugin.put(plugin.getId(), pluginInstallInfo);
        write(read);
        return pluginInstallInfo;
    }

    @Override
    public PluginInstallInfo update(PluginInfo plugin) throws IOException {
        Db read = read();
        PluginInstallInfo pluginInstallInfo = read.plugin.get(plugin.getId());
        pluginInstallInfo.setPluginInfo(plugin);
        read.plugin.put(plugin.getId(), pluginInstallInfo);
        write(read);
        return pluginInstallInfo;
    }

    @Override
    public PluginInstallInfo uninstall(String pluginId) throws IOException {
        Db read = read();
        PluginInstallInfo remove = read.plugin.remove(pluginId);
        write(read);
        return remove;
    }

    @Override
    public PluginInstallInfo stop(String pluginId) throws IOException {
        Db read = read();
        PluginInstallInfo pluginInstallInfo = read.plugin.get(pluginId);
        if (pluginInstallInfo.getStatus().equals(PluginStatus.STOP)) {
            log.warning(pluginId + "以处于停止状态");
        } else {
            pluginInstallInfo.setStatus(PluginStatus.STOP);
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String format = sdf1.format(new Date());
            pluginInstallInfo.setStatusChangeTime(format);
            write(read);
        }
        return pluginInstallInfo;
    }

    @Override
    public PluginInstallInfo start(String pluginId) throws IOException {
        Db read = read();
        PluginInstallInfo pluginInstallInfo = read.plugin.get(pluginId);
        if (pluginInstallInfo.getStatus().equals(PluginStatus.RUNNING)) {
            log.warning(pluginId + "以处于RUNNING状态");
        } else {
            pluginInstallInfo.setStatus(PluginStatus.RUNNING);
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String format = sdf1.format(new Date());
            pluginInstallInfo.setStatusChangeTime(format);
            write(read);
        }
        return pluginInstallInfo;
    }
    @Override
    public PluginInstallInfo get(String pluginId) throws IOException {
        Db read = read();
        PluginInstallInfo pluginInstallInfo = read.plugin.get(pluginId);
        return pluginInstallInfo;
    }

    @Override
    public void update(PluginInstallInfo pluginInstallInfo) throws IOException {
        Db read = read();
        PluginInstallInfo pluginInstallInfoDb = read.plugin.get(pluginInstallInfo.getPluginInfo().getId());
        pluginInstallInfoDb.setInitialization(pluginInstallInfo.getInitialization());
        pluginInstallInfoDb.setTemp(pluginInstallInfo.getTemp());
        pluginInstallInfoDb.setFilePath(pluginInstallInfo.getFilePath());
        pluginInstallInfoDb.setStatusChangeTime(pluginInstallInfo.getStatusChangeTime());
        write(read);
    }

    @Override
    public HashMap<String, PluginInstallInfo> getAll() throws IOException {
        Db read = read();
        return read.getPlugin();
    }


    public Db read() throws IOException {
        File file = new File(dbPath);
        FileInputStream fileInputStream = new FileInputStream(file);
        byte[] bytes = new byte[fileInputStream.available()];
        fileInputStream.read(bytes);
        fileInputStream.close();
        Db jsonObject = JSONObject.parseObject(new String(bytes, "utf-8"), Db.class);
        return jsonObject;
    }

    public void write(Db db) throws IOException {
        File file = new File(dbPath);
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        String s = JSON.toJSONString(db);
        fileOutputStream.write(s.getBytes("utf-8"));
        fileOutputStream.close();
    }

    public static class Db {
        public final String version = "v1.0";
        @Getter
        @Setter
        private HashMap<String, PluginInstallInfo> plugin = new HashMap<>();
    }
}
