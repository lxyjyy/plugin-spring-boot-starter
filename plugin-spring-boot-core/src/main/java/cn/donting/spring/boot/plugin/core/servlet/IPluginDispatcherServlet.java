package cn.donting.spring.boot.plugin.core.servlet;

import cn.donting.spring.boot.plugin.core.plugin.DoService;

/**
 * 插件的 DispatcherServlet接口
 * @author donting
 * 2020-09-25 14:17
 */
public interface IPluginDispatcherServlet extends DoService {
}
