package cn.donting.spring.boot.plugin.core.exception;

/**
 * @author donting
 * 2020-09-12 14:48
 */
public class PluginRuntimeException extends RuntimeException{
    public PluginRuntimeException() {
    }

    public PluginRuntimeException(String message) {
        super(message);
    }

    public PluginRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public PluginRuntimeException(Throwable cause) {
        super(cause);
    }

    public PluginRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
