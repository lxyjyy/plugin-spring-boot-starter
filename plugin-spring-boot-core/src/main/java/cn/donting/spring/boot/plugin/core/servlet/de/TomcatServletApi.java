package cn.donting.spring.boot.plugin.core.servlet.de;

import java.io.*;
import java.net.URL;
import java.util.jar.JarInputStream;
import java.util.zip.ZipEntry;

/**
 * @author donting
 * 2020-09-24 21:16
 */
public class TomcatServletApi extends AbsServletApiSeparate{

    @Override
    public void separate(String savePath, URL[] libs) {
        File file=new File(savePath);
        if (!file.exists()) {
            file.mkdirs();
        }
        try {
            JarInputStream zins = new JarInputStream(apiFileInputStream(libs));
            ZipEntry jarEntry = null;
            byte ch[] = new byte[256];
            while ((jarEntry = zins.getNextEntry()) != null) {
                File file1 = null;
                if (jarEntry.toString().startsWith("javax/servlet")) {
                    file1 = new File(savePath + File.separator + jarEntry.getName());
                }
                if (file1 == null) {
                    zins.closeEntry();
                    continue;
                }
                if (jarEntry.isDirectory()) {
                    file1.mkdirs();
                    zins.closeEntry();
                } else {
                    if (!file1.exists())
                        file1.createNewFile();
                    FileOutputStream fouts = new FileOutputStream(file1);
                    int i;
                    while ((i = zins.read(ch)) != -1)
                        fouts.write(ch, 0, i);
                    zins.closeEntry();
                    fouts.close();
                }
            }

        } catch (IOException exception) {
            exception.printStackTrace();
        }


    }

    @Override
    public InputStream apiFileInputStream(URL[] libs) throws IOException {
        for (URL libPath : libs) {
            if (libPath.getPath().indexOf("tomcat-embed-core")>-1) {
              return   libPath.openConnection().getInputStream();
            }
        }
        return null;
    }
}
