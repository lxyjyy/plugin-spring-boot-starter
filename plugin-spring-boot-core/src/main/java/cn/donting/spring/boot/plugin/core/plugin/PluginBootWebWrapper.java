package cn.donting.spring.boot.plugin.core.plugin;

import cn.donting.spring.boot.plugin.core.loader.classes.PluginClassLoader;
import cn.donting.spring.boot.plugin.core.plugin.info.PluginInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;

/**
 * @author donting
 * 2020-09-25 11:34
 */
public class PluginBootWebWrapper extends PluginBootWrapper implements DoService {

   private PluginWebSpringApplicationContext webApplicationContext;

    public PluginBootWebWrapper( File file, PluginInfo pluginInfo, Class<?> pluginMainClass, PluginClassLoader classLoader) {
        super( file, pluginInfo, pluginMainClass, classLoader);
    }

    @Override
    public void doService(HttpServletRequest request, HttpServletResponse response) throws Exception {
        webApplicationContext.doService(request,response);
    }


    @Override
    public void start(String[] args) throws Exception {
        super.start(args);
        webApplicationContext=(PluginWebSpringApplicationContext)super.applicationContext;
    }
}
