package cn.donting.spring.boot.plugin.core.util;

import java.io.*;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author donting
 * 2020-09-25 10:27
 */
public class Md5Util {
    public static String getFileMD5(File file) {
        BigInteger bigInt = null;
        try {
            FileInputStream fis = new FileInputStream(file);
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] buffer = new byte[1024];
            int length = -1;
            while ((length = fis.read(buffer, 0, 1024)) != -1) {
                md.update(buffer, 0, length);
            }
            bigInt = new BigInteger(1, md.digest());
        } catch (Exception  e) {
            e.printStackTrace();
        }
        return bigInt.toString(16);
    }

     public static String getInputStreamMD5(InputStream inputStream) {
        BigInteger bigInt = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] buffer = new byte[1024];
            int length = -1;
            while ((length = inputStream.read(buffer, 0, 1024)) != -1) {
                md.update(buffer, 0, length);
            }
            bigInt = new BigInteger(1, md.digest());
        } catch (Exception  e) {
            e.printStackTrace();
        }
        return bigInt.toString(16);
    }


}
