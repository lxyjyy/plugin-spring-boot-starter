package cn.donting.spring.boot.plugin.core;

import java.lang.annotation.*;

/**
 * 插件标识
 * @author donting
 * 2020-08-10 14:28
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface Plugin {
    String id();

    String name();

    String version();

    /**
     * 数字版本，升级时对比
     * @return
     */
    int versionCode();

    String description() default "";
}
