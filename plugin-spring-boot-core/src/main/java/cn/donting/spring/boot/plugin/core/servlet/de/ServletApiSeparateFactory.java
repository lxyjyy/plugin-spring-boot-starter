package cn.donting.spring.boot.plugin.core.servlet.de;

/**
 * @author donting
 * 2020-09-24 21:16
 */
public class ServletApiSeparateFactory {

    public  static ServletApiSeparate getServletApiSeparate(){

        return new TomcatServletApi();
    }
}
