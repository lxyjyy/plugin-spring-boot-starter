package cn.donting.spring.boot.plugin.core.logging;

import java.util.HashMap;

/**
 * @author donting
 * 2020-09-12 20:29
 */
public class LogFactory {
    private static HashMap<Class, Log> hashMap = new HashMap<>();
    public static Log getLogger(Class c) {
        if (hashMap.containsKey(c)) {
            return hashMap.get(c);
        }
        Log logger = new Log(c);
        hashMap.put(c, logger);
        return logger;
    }
}
