package cn.donting.spring.boot.plugin.core.server;

import cn.donting.spring.boot.plugin.core.loader.classes.PluginClassLoader;
import cn.donting.spring.boot.plugin.core.loader.plugin.dev.DevManagerLoader;
import cn.donting.spring.boot.plugin.core.plugin.PluginWrapper;
import cn.donting.spring.boot.plugin.core.plugin.manger.PluginManager;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * 服务的启动类
 *
 * @author donting
 * 2020-09-24 21:30
 */
public class PluginServerApplication {


    private static void loaderManager(String[] args) throws IOException, ReflectiveOperationException {
        //启动manger
        File file=new File(PluginClassLoader.DEV_PLUGIN_MANAGER_CLASSES);
        if(!file.exists()){
            throw new RuntimeException("not find DEV_PLUGIN_MANAGER_CLASSES:"+PluginClassLoader.DEV_PLUGIN_MANAGER_CLASSES);
        }
        new DevManagerLoader().run(file);
    }

    public static void runServer(String[] args) throws Exception {
        URLClassLoader systemClassLoader = (URLClassLoader)ClassLoader.getSystemClassLoader();
        loaderManager(args);
    }

}
