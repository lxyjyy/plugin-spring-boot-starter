package cn.donting.spring.boot.plugin.core.plugin;

import cn.donting.spring.boot.plugin.core.exception.PluginException;
import cn.donting.spring.boot.plugin.core.plugin.info.PluginInfo;

import java.lang.annotation.Annotation;

/**
 * 插件标识
 * manager 也可以使用
 * @see DefaultPluginMake
 * @see cn.donting.spring.boot.plugin.core.Plugin
 * @author donting
 * 2020-09-24 13:32
 */
public interface PluginInfoMake {

    PluginInfo getPluginInfo(Class<?> mainClass) throws PluginException;
}
