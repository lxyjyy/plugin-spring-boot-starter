package cn.donting.spring.boot.plugin.core.exception;

/**
 * @author donting
 * 2020-09-22 11:38
 */
public class PluginMsgException extends PluginException {
    public PluginMsgException() {
    }

    public PluginMsgException(String message) {
        super(message);
    }

    public PluginMsgException(String message, Throwable cause) {
        super(message, cause);
    }

    public PluginMsgException(Throwable cause) {
        super(cause);
    }

    public PluginMsgException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
