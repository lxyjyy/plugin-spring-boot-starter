package cn.donting.spring.boot.plugin.core.plugin;

import cn.donting.spring.boot.plugin.core.Plugin;
import cn.donting.spring.boot.plugin.core.exception.PluginException;
import cn.donting.spring.boot.plugin.core.plugin.info.PluginInfo;

/**
 * @author donting
 * 2020-09-24 13:36
 */
public class DefaultPluginMake implements PluginInfoMake {


    @Override
    public PluginInfo getPluginInfo(Class<?> mainClass) throws PluginException {
        Plugin annotation = mainClass.getAnnotation(Plugin.class);
        if(annotation==null){
            throw new PluginException("not find Plugin annotation");
        }
        PluginInfo pluginInfo = new PluginInfo(annotation);
        return pluginInfo;
    }
}
