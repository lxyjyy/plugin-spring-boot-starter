package cn.donting.spring.boot.plugin.core.plugin.info;

import cn.donting.spring.boot.plugin.core.Plugin;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

/**
 * @author donting
 * 2020-09-08 13:57
 */
@Getter
public class PluginInfo {
    private String id;
    private String name;
    private String version;
    private int versionCode;
    @Setter
    private String springbootVersion;
    @Setter
    private String description;

    public PluginInfo(Plugin plugin) {
        id=plugin.id();
        name=plugin.name();
        version=plugin.version();
        versionCode=plugin.versionCode();
        description=plugin.description();
    }
    public PluginInfo(String id, String name, String version, int versionCode) {
        this.id = id;
        this.name = name;
        this.version = version;
        this.versionCode = versionCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PluginInfo that = (PluginInfo) o;
        return versionCode == that.versionCode &&
                id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, versionCode);
    }

}
