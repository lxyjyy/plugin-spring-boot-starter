package cn.donting.spring.boot.plugin.core.plugin;

import cn.donting.spring.boot.plugin.core.plugin.info.PluginInfo;

/**
 * 插件实体
 * @author donting
 * 2020-09-25 11:14
 */
public interface PluginWrapper extends PluginSpringApplicationContext,TempDirectory{

    PluginInfo getPluginInfo();

    String getId();

    void start(String[] args) throws Exception;

    void stop() throws Exception;

    void uninstall() throws Exception;


    PluginStatus getStatus();

}
