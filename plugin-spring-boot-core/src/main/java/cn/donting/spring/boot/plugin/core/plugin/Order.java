package cn.donting.spring.boot.plugin.core.plugin;

/**
 * 越大越靠前
 * @author donting
 * 2020-09-28 16:15
 */
public interface Order {
    default int order(){return 1;}
}
